from django.contrib import admin
from athletes_CRUD.models import Athlete

class AthleteAdmin(admin.ModelAdmin):
    pass

admin.site.register(Athlete, AthleteAdmin)
