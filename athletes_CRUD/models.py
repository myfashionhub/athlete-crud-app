from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.contrib.auth.models import User

class Athlete(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    sport = models.CharField(max_length=50)
    league = models.CharField(max_length=100)
    team = models.CharField(max_length=50)
    retired = models.BooleanField(default=True)
    current_position = models.CharField(max_length=100)
    image = models.CharField(max_length=500)

    def get_absolute_url(self):
        return reverse('athlete_form', kwargs={'pk': self.pk})
