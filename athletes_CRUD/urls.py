from django.conf import settings
from django.conf.urls import patterns, include, url
from athletes_CRUD import views

from django.contrib.staticfiles.views import serve
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.index),
    url(r'^login$', views.user_login, name='login'),
    url(r'^register$', views.register, name='register'),
    url(r'^logout$', views.user_logout, name='logout'),

    url(r'^athletes$', views.AthleteList.as_view(), name='athlete_list'),
    url(r'^new-athlete$', views.AthleteCreate.as_view(), name='athlete_form'),
    url(r'^edit-athlete/(?P<pk>\d+)$', views.AthleteUpdate.as_view(), name='athlete_update'),
    url(r'^delete-athlete/(?P<pk>\d+)$', views.AthleteDelete.as_view(), name='athlete_confirm_delete'),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
