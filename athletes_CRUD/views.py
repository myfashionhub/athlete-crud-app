from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy

from django.template import RequestContext
from django.template.defaulttags import csrf_token
from django.contrib.auth import authenticate, login, logout

from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from athletes_CRUD.forms import UserForm
from athletes_CRUD.models import Athlete

# User auth

def index(request):
    return render(request, 'index.html', {})

def user_login(request):
    context = RequestContext(request)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid credentails.")
    else:
        return render('index.html', 'login.html', context)

def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

def register(request):
    context = RequestContext(request)
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            user = authenticate(username=request.POST['username'], password=request.POST['password'])
            login(request, user)
            registered = True
            return HttpResponseRedirect('/')
        else:
            print user_form.errors
    else:
        user_form = UserForm()
    return render_to_response('register.html', {'user_form': user_form, 'registered': registered}, context)

# Athlete CRUD
class AthleteList(ListView):
    model = Athlete

class AthleteCreate(CreateView):
    model = Athlete
    success_url = reverse_lazy('athlete_list')

class AthleteUpdate(UpdateView):
    model = Athlete
    success_url = reverse_lazy('athlete_list')
    template_name_suffix = '_update_form'

class AthleteDelete(DeleteView):
    model = Athlete
    success_url = reverse_lazy('athlete_list')

